<?php /*a:2:{s:85:"/www/wwwroot/dati.sdwanyue.com/public/themes/admin_simpleboot3/admin/paper/index.html";i:1646978668;s:81:"/www/wwwroot/dati.sdwanyue.com/public/themes/admin_simpleboot3/public/header.html";i:1646978690;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
	<ul class="nav nav-tabs">
		<li class="active"><a href="<?php echo url('paper/index'); ?>">问卷调查</a></li>
		<li><a href="<?php echo url('paper/add'); ?>">添加</a></li>
	</ul>


	<form method="post" class="js-ajax-form margin-top-20" action="<?php echo url('paper/listOrder'); ?>">



		<div class="table-actions">
			<button class="btn btn-primary btn-sm js-ajax-submit" type="submit"><?php echo lang('SORT'); ?></button>
		</div>
		<table class="table table-hover table-bordered margin-top-20">
			<thead>
			<tr>
				<th width="50"><?php echo lang('SORT'); ?></th>
				<th width="50">ID</th>
				<th>标题</th>
				<th>结束时间</th>
				<th>状态</th>

				<th width="320"><?php echo lang('ACTIONS'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php 

			 if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
				<tr>
					<td>
						<input name="list_orders[<?php echo $vo['id']; ?>]" class="input-order" type="text" value="<?php echo $vo['list_order']; ?>">
					</td>
					<td><?php echo $vo['id']; ?></td>
					<td><?php echo $vo['title']; ?></td>
					<td><?php echo date('Y-m-d H:i:s',$vo['endtime']); ?></td>
					<td><?php echo $status[$vo['status']]; ?></td>
					<td>
						<a class="btn btn-xs btn-primary" href="javascript:parent.openIframeLayer('<?php echo url('ask/index',array('listid'=>$vo['id'],'status'=>$vo['status'])); ?>')">问题列表</a>

						<a class="btn btn-xs btn-success" href="javascript:parent.openIframeLayer('<?php echo url('userask/index',array('listid'=>$vo['id'])); ?>')">问卷统计</a>

						<?php if($vo['status'] == 0): ?>
							<a class="btn btn-xs btn-primary js-ajax-dialog-btn" data-msg="启用之后，该问卷将线上生效，确定操作？" href="<?php echo url('paper/setstatus',array('id'=>$vo['id'])); ?>">上线启用</a>
						<?php endif; if($vo['status'] == 1): ?>

							<a class="btn btn-xs btn-danger js-ajax-dialog-btn" data-msg="禁用用之后，该问卷将不在线上展示，确定操作？"  href="<?php echo url('paper/cancelstatus',array('id'=>$vo['id'])); ?>">终止禁用</a>
						<?php endif; if($vo['status'] == 0 || $vo['status'] == 2): ?>
							<a class="btn btn-xs btn-primary" href="<?php echo url('paper/edit',array('id'=>$vo['id'])); ?>"><?php echo lang('EDIT'); ?></a>
							<a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('paper/delete',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
						<?php endif; ?>

					</td>
				</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>

			</tbody>
		</table>
	</form>
	<div class="pagination"><?php echo $page; ?></div>
</div>
<script src="/static/js/admin.js"></script>
</body>
</html>